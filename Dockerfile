##############
# Base image #
##############
FROM ubuntu:rolling as base

ENV PYTHON=python3.7

RUN \
    # Disable recommended and suggested packages
    echo 'APT::Install-Recommends "0";' > /etc/apt/apt.conf.d/20-no-recommends && \
    echo 'APT::Install-Suggests "0";' > /etc/apt/apt.conf.d/20-no-suggests && \
    apt-get update && \
    apt-get upgrade --assume-yes && \
    apt-get install --assume-yes \
        ${PYTHON} \
        python3-pip \
    && \
    apt-get clean all && \
    rm -rf /var/lib/apt/lists/*


###########
# Builder #
###########
FROM base as builder

# Install system dependencies
RUN \
    apt-get update && \
    apt-get install --assume-yes \
        cmake \
        make \
        g++ \
        libcairo2-dev \
        libgirepository1.0-dev \
        libgrpc++-dev \
        libgmock-dev \
        libgtest-dev \
        libprotobuf-dev \
        libssl-dev \
        pkg-config \
        protobuf-compiler \
        protobuf-compiler-grpc \
        python3-dev \
        python3-setuptools \
        python3-wheel \
        uuid-dev

# Build and install buildbox-common
COPY buildbox-common /build/buildbox-common
RUN \
    cd /build/buildbox-common && \
    cmake . -Bbuild && \
    make -C build -j $(nproc) && \
    make -C build install && \
    rm -rf /build

# Build and copy buildbox-casd
COPY buildbox-casd /build/buildbox-casd
RUN \
    cd /build/buildbox-casd && \
    cmake . -Bbuild -DBUILDBOX_CASD_VERSION=0.0.6 && \
    make -C build -j $(nproc) && \
    make -C build DESTDIR=/artifacts/ install && \
    rm -rf /build

# Build and copy buildbox-run-bubblewrap
COPY buildbox-run-bubblewrap /build/buildbox-run-bubblewrap
RUN \
    cd /build/buildbox-run-bubblewrap && \
    cmake . -Bbuild && \
    make -C build -j $(nproc) && \
    make -C build DESTDIR=/artifacts/ install && \
    rm -rf /build

# Build wheels for BuildStream and dependencies
COPY buildstream /build/buildstream
COPY bst-plugins-experimental /build/bst-plugins-experimental
RUN \
    ${PYTHON} -m pip wheel --wheel-dir /artifacts/wheels \
        /build/buildstream \
        /build/bst-plugins-experimental \
        PyGobject \
        pytoml

#####################
# BuildStream Image #
#####################
FROM base as buildstream

COPY --from=builder /artifacts /artifacts

RUN \
    apt-get update && \
    apt-get install --assume-yes \
        # Utilities
        tmux \
        vim \
        # BuildStream
        bubblewrap \
        # Plugins
        git \
        patch \
        lzip \
        gir1.2-ostree-1.0 \
        # Buildbox
        libgrpc++ \
        libprotobuf17 \
    && \
    apt-get clean all && \
    # Install wheels
    ${PYTHON} -m pip install /artifacts/wheels/* && \
    # Install buildbox-casd
    cp /artifacts/usr/local/bin/buildbox-casd /usr/local/bin/buildbox-casd && \
    # Install buildbox-run-bubblewrap
    cp /artifacts/usr/local/bin/buildbox-run-bubblewrap /usr/local/bin/buildbox-run && \
    # Ensure they all work
    bst --version && \
    buildbox-casd --version && \
    # buildbox-run-bubblewrap doesn't have a version
    buildbox-run --help && \
    rm -r /artifacts && \
    rm -rf /var/lib/apt/lists/* && \
    useradd -m buildstream

# Override Bst version, which is unavailable due to git missing
ADD files/bst-version.py /usr/local/lib/python3.7/dist-packages/buildstream/_version.py

ENV BST_FORCE_SANDBOX="buildbox-run"
USER buildstream
WORKDIR /home/buildstream/project
